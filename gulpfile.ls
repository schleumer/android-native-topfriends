require! 'gulp'

image-resize = require 'gulp-image-resize'
 
gulp.task 'mipmap-hdpi' ->
  gulp.src './app/src/main/res/src-drawable/**/*.png'
    .pipe (image-resize {
      width: 72
      height: 72
      crop: true
      upscale: false
    })
    .pipe (gulp.dest './app/src/main/res/mipmap-hdpi')

gulp.task 'mipmap-mdpi' ->
  gulp.src './app/src/main/res/src-drawable/**/*.png'
    .pipe (image-resize {
      width: 48
      height: 48
      crop: true
      upscale: false
    })
    .pipe (gulp.dest './app/src/main/res/mipmap-mdpi')

gulp.task 'mipmap-xhdpi' ->
  gulp.src './app/src/main/res/src-drawable/**/*.png'
    .pipe (image-resize {
      width: 96
      height: 96
      crop: true
      upscale: false
    })
    .pipe (gulp.dest './app/src/main/res/mipmap-xhdpi')

gulp.task 'mipmap-xxhdpi' ->
  gulp.src './app/src/main/res/src-drawable/**/*.png'
    .pipe (image-resize {
      width: 144
      height: 144
      crop: true
      upscale: false
    })
    .pipe (gulp.dest './app/src/main/res/mipmap-xxhdpi')


gulp.task 'default' [
  \mipmap-hdpi
  \mipmap-mdpi
  \mipmap-xhdpi
  \mipmap-xxhdpi
]
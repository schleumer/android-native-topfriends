package li.ues.topfriends;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Wesley on 14/01/2016.
 */
public class GenericActivity extends Activity {
    protected Tracker mTracker = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TheApplication application = (TheApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mTracker.setScreenName("Loaded~" + this.getClass().getName());
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void logAction(String action) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction(action)
                .build());
    }

    public void logError(String type) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Error")
                .setAction(type)
                .build());
    }
}

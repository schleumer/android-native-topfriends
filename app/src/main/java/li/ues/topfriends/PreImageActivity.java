package li.ues.topfriends;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import li.ues.topfriends.system.*;

import li.ues.topfriends.system.Thread;

class Participants extends ArrayList<User> {
    public User from(final String fbid) {
        return Iterables.tryFind(this, new Predicate<User>() {
            @Override
            public boolean apply(User input) {
                return input.getId().equals(fbid) || input.getFbId().equals(fbid);
            }
        }).orNull();
    }
    public List<User> from(JSONArray arrayOfFbids) throws JSONException {
        List<User> finalUsers = new ArrayList<>();
        for(int i = 0; i < arrayOfFbids.length(); i++) {
            final String fbid = arrayOfFbids.getString(i);
            finalUsers.add(Iterables.tryFind(this, new Predicate<User>() {
                @Override
                public boolean apply(User input) {
                    return input.getId().equals(fbid);
                }
            }).orNull());
        }

        return finalUsers;
    }
}

public class PreImageActivity extends GenericActivity {

    protected final void setStatus(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView status = (TextView) findViewById(R.id.status);
                status.setText(Html.fromHtml(str));
            }
        });
    }

    // THE WRONGNESS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_image);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (BuildConfig.DEBUG) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("98D19B841C553EFE8386D5D98A966B43").build();
            mAdView.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        ImageView logo = (ImageView) findViewById(R.id.logo);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.spin);

        logo.setAnimation(animation);


        setStatus(getString(R.string.pre_image_status2));

        AsyncTask<Void, Void, ImageMeta> task = new AsyncTask<Void, Void, ImageMeta>() {
            int retries = 0;

            @Override
            protected ImageMeta doInBackground(Void... params) {
                if (retries > 3) {
                    setStatus(getString(R.string.pre_image_retries_exceeded));
                    logError("3 erros ao contactar o Facebook");
                    return null;
                }
                String contentX = Utils.rawPostUrl("https://www.facebook.com" +
                                "/ajax/mercury/threadlist_info.php",
                        Utils.TYPE_FORM,
                        "client=jewel" +
                                "&inbox[offset]=0" +
                                "&inbox[limit]=25" +
                                "&inbox[filter]" +
                                "&__a=1" +
                                "&fb_dtsg=" + MainActivity.userData.FbDtsg);
                setStatus(getString(R.string.pre_image_status3));

                if(contentX == null) {
                    retries += 1;
                    setStatus(String.format(getString(R.string.pre_image_status4), retries));
                    logError("Erro ao contactar o Facebook");
                    return doInBackground(params);
                }

                if (
                        contentX.contains("\"error\":1545010")
                                || contentX.contains("\"errorSummary\":\"Messages Unavailable\"")) {
                    retries += 1;
                    setStatus(String.format(getString(R.string.pre_image_status4), retries));
                    logError("Erro no resultado do Facebook");
                    return doInBackground(params);
                }

                try {
                    JSONObject resultObj = new JSONObject(contentX.replaceAll("^for.*?\\{", "{"));
                    JSONObject payloadObj = resultObj.getJSONObject("payload");
                    JSONArray threadsArr = payloadObj.getJSONArray("threads");
                    JSONArray participantsArr = payloadObj.getJSONArray("participants");
                    Participants participants = new Participants();
                    List<Thread> threads = new ArrayList<>();

                    for(int i = 0; i < participantsArr.length(); i++) {
                        JSONObject userObj = participantsArr.getJSONObject(i);
                        User user = new User();
                        user.setFbId(userObj.getString("fbid"));
                        user.setId(userObj.getString("id"));
                        user.setImageUrl("http://graph.facebook.com/" + user.getFbId() + "/picture?width=120");
                        user.setName(userObj.getString("name"));
                        user.setShortName(userObj.getString("short_name"));

                        //user.setImageUrl("http://lorempixel.com/200/200/cats?i=" + String.valueOf(i));
                        //user.setName("Gatinho(a) #" + String.valueOf(i));
                        //user.setShortName("Gatinho(a)");

                        participants.add(user);
                    }

                    for(int i = 0; i < threadsArr.length(); i++) {
                        JSONObject threadObj = threadsArr.getJSONObject(i);
                        Thread thread = new Thread();
                        thread.setMessageCount(threadObj.getLong("message_count"));
                        thread.setOtherUserId(threadObj.getString("other_user_fbid"));
                        thread.setParticipants(participants.from(threadObj.getJSONArray("participants")));
                        thread.setThreadFbId(threadObj.getString("thread_fbid"));
                        thread.setThreadId(threadObj.getString("thread_id"));
                        if(!threadObj.isNull("other_user_fbid")) {
                            thread.setOtherUser(participants.from(threadObj.getString("other_user_fbid")));
                            threads.add(thread);
                        }
                    }


                    threads = Lists.newArrayList(Iterables.filter(threads, new Predicate<Thread>() {
                        @Override
                        public boolean apply(Thread input) {
                            return input.getOtherUser() != null;
                        }
                    }));

                    logAction("Dados do Facebook carregados");

                    ImageMeta meta = new ImageMeta(threads);
                    Intent intent = new Intent(PreImageActivity.this, ConfigurarImagemActivity.class);
                    intent.putExtra("data", meta);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

        task.execute();

    }

}
package li.ues.topfriends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

import java.io.Serializable;
import java.util.List;

import li.ues.topfriends.system.*;
import li.ues.topfriends.system.Thread;

interface OnItemClickListener<T> {
    void onClick(View view, T item);
}

class ArrayAdapterItem extends ArrayAdapter<Thread> {

    Context mContext;
    int layoutResourceId;
    List<Thread> data = null;
    OnItemClickListener<Thread> listener = null;

    public ArrayAdapterItem(Context mContext, int layoutResourceId, List<Thread> data) {
        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener<Thread> listener) {
        this.listener = listener;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        /*
         * The convertView argument is essentially a "ScrapView" as described is Lucas post
         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
         * It will have a non-null value when ListView is asking you recycle the row layout.
         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
         */
        if (v == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            v = inflater.inflate(layoutResourceId, parent, false);
            v.setTag(R.id.thread_picture, v.findViewById(R.id.thread_picture));
            v.setTag(R.id.thread_counter, v.findViewById(R.id.thread_counter));
            v.setTag(R.id.thread_name, v.findViewById(R.id.thread_name));
        }

        // object item based on the position
        final Thread objectItem = data.get(position);

        ImageView profilePicture = (ImageView) v.getTag(R.id.thread_picture);
        TextView counter = (TextView) v.getTag(R.id.thread_counter);
        TextView name = (TextView) v.getTag(R.id.thread_name);

        name.setText(objectItem.getOtherUser().getShortName());
        counter.setText(Utils.plural(mContext, objectItem.getMessageCount(), R.string.sing_messages, R.string.plur_messages, R.string.none_messages));

        Glide.with(mContext)
                .load(objectItem.getOtherUser().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .transform(new CircleTransform(mContext))
                .placeholder(R.drawable.logo)
                .crossFade()
                .into(profilePicture);

//        // get the TextView and then set the text (item name) and tag (item ID) values
//        TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
//        textViewItem.setText(objectItem.itemName);
//        textViewItem.setTag(objectItem.itemId);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(view, objectItem);
                }
            }
        });

        return v;

    }

    public List<Thread> getItems() {
        return data;
    }

}

class WizardPagerAdapter extends PagerAdapter {

    private Context mContext = null;

    public WizardPagerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        //((ViewPager) container).removeView((View) object);
    }

    public Object instantiateItem(ViewGroup collection, int position) {
        int resId = 0;
        switch (position) {
            case 0:
                resId = R.id.configurar_page_1;
                break;
            case 1:
                resId = R.id.configurar_page_2;
                break;
            case 2:
                resId = R.id.configurar_page_3;
                break;
        }
        return collection.findViewById(resId);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.configurar_tab_configurar);
            case 1:
                return mContext.getString(R.string.configurar_tab_selecionados);
            default:
                return mContext.getString(R.string.configurar_tab_removidos);
        }
    }
}

public class ConfigurarImagemActivity extends GenericActivity {

    private ImageMeta meta = null;

    private ArrayAdapterItem adapter1 = null;
    private ArrayAdapterItem adapter2 = null;

    // 3(4 colunas), 4(3 colunas) ou 6(2 colunas)
    private String columnSize = "6";
    private String maxFriends = "10";
    private String mostrarRanking = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar_imagem);

        logAction("Configurar imagem acessado");

        Intent intent = getIntent();
        try {
            meta = (ImageMeta) intent.getSerializableExtra("data");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final GridView list = (GridView) findViewById(R.id.thread_list);
        GridView removedList = (GridView) findViewById(R.id.thread_list_removed);
        Button finalizar = (Button) findViewById(R.id.finalizar);

        adapter1 = new ArrayAdapterItem(this, R.layout.thread_view, meta.getThreads());
        adapter2 = new ArrayAdapterItem(this, R.layout.thread_view, meta.getRemovedThreads());

        adapter2.setOnItemClickListener(new OnItemClickListener<Thread>() {
            @Override
            public void onClick(View view, Thread item) {
                adapter1.add(item);
                adapter1.sort(new Ordering<Thread>() {
                    @Override
                    public int compare(Thread left, Thread right) {
                        return Longs.compare(left.getMessageCount(), right.getMessageCount());
                    }
                }.reverse());
                adapter2.remove(item);
            }
        });

        adapter1.setOnItemClickListener(new OnItemClickListener<Thread>() {
            @Override
            public void onClick(View view, Thread item) {
                adapter2.add(item);
                adapter2.sort(new Ordering<Thread>() {
                    @Override
                    public int compare(Thread left, Thread right) {
                        return Longs.compare(left.getMessageCount(), right.getMessageCount());
                    }
                }.reverse());
                adapter1.remove(item);
            }
        });

        list.setAdapter(adapter1);
        removedList.setAdapter(adapter2);


        WizardPagerAdapter adapter = new WizardPagerAdapter(this);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);

        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logAction("Finalizar clicado");
                Intent intent = new Intent(ConfigurarImagemActivity.this, FinalizarActivity.class);
                intent.putExtra("data", (Serializable) adapter1.getItems());
                intent.putExtra("columnSize", columnSize);
                intent.putExtra("maxFriends", maxFriends);
                intent.putExtra("mostrarRanking", mostrarRanking);
                startActivity(intent);
                finish();
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.duas_colunas:
                if (checked)
                    this.columnSize = "6";
                    break;
            case R.id.tres_colunas:
                if (checked)
                    this.columnSize = "4";
                break;
            case R.id.quatro_colunas:
                if (checked)
                    this.columnSize = "3";
                break;
        }
    }

    public void onMaxFriendsClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.cinco_amigos:
                if (checked)
                    this.maxFriends = "5";
                break;
            case R.id.dez_amigos:
                if (checked)
                    this.maxFriends = "10";
                break;
            case R.id.quinze_amigos:
                if (checked)
                    this.maxFriends = "15";
                break;
            case R.id.vinte_amigos:
                if (checked)
                    this.maxFriends = "20";
                break;
            case R.id.vinte_e_cinco_amigos:
                if (checked)
                    this.maxFriends = "25";
                break;
        }
    }

    public void onShowRankClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.mostrar_ranking_sim:
                if(checked)
                    this.mostrarRanking = "1";
                break;
            case R.id.mostrar_ranking_nao:
                if(checked)
                    this.mostrarRanking = "0";
        }
    }
}

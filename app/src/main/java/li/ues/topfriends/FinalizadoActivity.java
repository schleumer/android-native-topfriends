package li.ues.topfriends;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class FinalizadoActivity extends GenericActivity {
    File localFile = null;
    Bitmap localImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalizado);

        Intent intent = getIntent();
        final String url = intent.getStringExtra("url");
        final ImageView image = (ImageView) findViewById(R.id.imagem);
        final Button compartilhar = (Button) findViewById(R.id.compartilhar);
        final Button visualizar = (Button) findViewById(R.id.visualizar);
        final LinearLayout loading = (LinearLayout) findViewById(R.id.loading);
        final ScrollView imagemLayout = (ScrollView) findViewById(R.id.imagem_layout);

        compartilhar.setEnabled(false);
        visualizar.setEnabled(false);

        compartilhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logAction("Apertou compartilhar");
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, getString(R.string.finalizado_compartilhar));
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        values);

                OutputStream outstream;
                try {
                    outstream = getContentResolver().openOutputStream(uri);
                    localImage.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
                    outstream.close();
                } catch (Exception e) {
                    System.err.println(e.toString());
                }

                share.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(share, getString(R.string.finalizado_compartilhar_imagem)));
            }
        });

        visualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logAction("Apertou visualizar");
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, getString(R.string.finalizado_visualizar));
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        values);

                OutputStream outstream;
                try {
                    outstream = getContentResolver().openOutputStream(uri);
                    localImage.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
                    outstream.close();
                } catch (Exception e) {
                    System.err.println(e.toString());
                }

                Intent view = new Intent(Intent.ACTION_VIEW);
                view.setDataAndType(uri, "image/*");
                startActivity(view);
            }
        });

        AsyncTask<Void, Void, Void> downloadImage = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                InputStream in = null;
                URL uri = null;
                try
                {
                    uri = new URL(url);
                    URLConnection urlConn = uri.openConnection();
                    HttpURLConnection httpConn = (HttpURLConnection) urlConn;
                    httpConn.connect();
                    in = httpConn.getInputStream();
                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

                localImage = BitmapFactory.decodeStream(in);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        image.setImageBitmap(localImage);
                        compartilhar.setEnabled(true);
                        visualizar.setEnabled(true);
                        loading.setVisibility(View.GONE);
                        imagemLayout.setVisibility(View.VISIBLE);
                    }
                });

                return null;
            }
        };

        downloadImage.execute();
    }

}

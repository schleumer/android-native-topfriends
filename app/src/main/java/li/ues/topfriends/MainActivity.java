package li.ues.topfriends;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import li.ues.topfriends.system.CircleTransform;
import li.ues.topfriends.utils.CrawUserInfo;
import li.ues.topfriends.utils.UserData;

class AsyncVerificationOfUserLoggedInOnFacebook extends AsyncTask<String, Integer, UserData> {

    @Override
    protected UserData doInBackground(String... params) {
        publishProgress(1);
        String text = Utils.getUrl("http://m.facebook.com/");

        publishProgress(2);
        return Utils.isUserOnHtml(text);
    }
}


public class MainActivity extends GenericFragmentActivity {
    Integer STATE_OK = 3;
    Integer STATE_REGISTER = 2;
    Integer STATE_LOADING = 1;
    public static UserData userData;

    private void fsm(Integer state) {
        fsm(state, null);
    }

    private void fsm(Integer state, String message) {
        if (this.isFinishing()) {
            return;
        }
        LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.home_frame_loading);
        ImageView loadingLogo = (ImageView) findViewById(R.id.home_loading_image);
        TextView loadingMessage = (TextView) findViewById(R.id.home_loading_message);
        FrameLayout registerLayout = (FrameLayout) findViewById(R.id.home_frame_register);
        TextView registerMessage = (TextView) findViewById(R.id.home_register_message);
        FrameLayout okLayout = (FrameLayout) findViewById(R.id.home_frame_ok);
        TextView okMessage = (TextView) findViewById(R.id.home_ok_message);

        okLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.GONE);

        loadingLogo.clearAnimation();

        Spanned text;
        if (message != null) {
            text = Html.fromHtml(message);
        } else {
            text = Html.fromHtml("");
        }

        switch (state) {
            case 3: {
                TextView homeUserName = (TextView) findViewById(R.id.home_ok_user_name);
                ImageView homeUserImage = (ImageView) findViewById(R.id.home_ok_user_image);
                if (userData != null && userData.exists()) {
                    homeUserName.setText(userData.getName());
                    //homeUserName.setText("Estagiário");
                    try {
                        Glide.with(this)
                                .load(userData.getProfilePicture())
                                .fitCenter()
                                .transform(new CircleTransform(this))
                                .placeholder(R.drawable.logo)
                                .crossFade()
                                .into(homeUserImage);
                    } catch (Exception ex) {
                        logError("Carregar imagem de perfil");
                        ex.printStackTrace();
                    }
                }
                okMessage.setText(text);
                okLayout.setVisibility(View.VISIBLE);
                break;
            }
            case 2: {
                registerMessage.setText(text);
                registerLayout.setVisibility(View.VISIBLE);
                break;
            }
            default: {
                /*loadingLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.spin));*/
                loadingMessage.setText(text);
                loadingLayout.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logAction("Aplicativo acessado");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (BuildConfig.DEBUG) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("98D19B841C553EFE8386D5D98A966B43").build();
            mAdView.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        CookieSyncManager.createInstance(this);

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        String loggedUser = sharedPref.getString("LOGGED_USER", null);

        if (loggedUser != null) {
            AsyncVerificationOfUserLoggedInOnFacebook task = new AsyncVerificationOfUserLoggedInOnFacebook() {
                @Override
                protected void onPostExecute(UserData data) {
                    if (data.isOk()) {
                        CrawUserInfo crawTask = new CrawUserInfo() {
                            @Override
                            protected void onPostExecute(final UserData availableData) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (availableData.exists()) {
                                            userData = availableData;
                                            fsm(STATE_OK);
                                        } else {
                                            fsm(STATE_REGISTER);
                                        }
                                    }
                                });
                            }
                        };
                        crawTask.execute(data);
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fsm(STATE_REGISTER);
                            }
                        });
                    }
                }

                @Override
                protected void onProgressUpdate(final Integer... values) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Integer state = values[0];
                            if (state == 1) {
                                fsm(STATE_LOADING, getString(R.string.main_acessando_facebook));
                            } else if (state == 2 || state == 3) {
                                fsm(STATE_LOADING, getString(R.string.main_procurando_voce));
                            } else {
                                fsm(STATE_LOADING, getString(R.string.main_erro_inesperado));
                            }
                        }
                    });
                    super.onProgressUpdate(values);
                }
            };
            task.execute();

            fsm(STATE_LOADING);
        } else {
            fsm(STATE_REGISTER);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0xB16B00B5) {
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            sharedPref.edit().putString("LOGGED_USER", data.toUri(Intent.URI_INTENT_SCHEME)).apply();
            userData = new UserData(data);
            CrawUserInfo crawTask = new CrawUserInfo() {
                @Override
                protected void onPostExecute(final UserData availableData) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (availableData.exists()) {
                                userData = availableData;
                                fsm(STATE_OK);
                            } else {
                                fsm(STATE_REGISTER);
                            }
                        }
                    });
                }
            };
            crawTask.execute(userData);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void entrarNoFacebook(View view) {
        Intent intent = new Intent(this, EntrarNoFacebookActivity.class);
        startActivityForResult(intent, 10);
    }

    public void preImage(View view) {
        Intent intent = new Intent(this, PreImageActivity.class);
        startActivity(intent);
    }
}

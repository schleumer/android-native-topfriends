package li.ues.topfriends;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.Html;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import li.ues.topfriends.system.Thread;

public class FinalizarActivity extends GenericActivity {
    protected final void setStatus(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView status = (TextView) findViewById(R.id.status);
                status.setText(Html.fromHtml(str));
            }
        });
    }

    protected final void setStatus2(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView status = (TextView) findViewById(R.id.status2);
                status.setText(Html.fromHtml(str));
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalizar);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        if (BuildConfig.DEBUG) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("98D19B841C553EFE8386D5D98A966B43").build();
            mAdView.loadAd(adRequest);
        } else {
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        ImageView logo = (ImageView) findViewById(R.id.logo);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.spin);
        logo.setAnimation(animation);

        final Intent intent = getIntent();

        setStatus(getString(R.string.finalizar_status1));

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            private int retries = 0;
            @Override
            protected Void doInBackground(Void... voids) {
                List<?> threads = null;
                if(intent.getSerializableExtra("data") instanceof List) {
                    threads = (List<?>) intent.getSerializableExtra("data");
                }

                String maxFriends = intent.getStringExtra("maxFriends");
                String columnSize = intent.getStringExtra("columnSize");
                String mostrarRanking = intent.getStringExtra("mostrarRanking");

                Gson gson = new Gson();
                String json = gson.toJson(threads);

                setStatus(getString(R.string.finalizar_status2));

                String content = Utils.rawPostUrl(
                        getString(R.string.server_url) + "/v1" +
                                "?me=" + MainActivity.userData.getId() +
                                "&columnSize=" + columnSize +
                                "&maxFriends=" + maxFriends +
                                "&showRanking=" + mostrarRanking +
                                "&lang=" + getResources().getConfiguration().locale.getLanguage(),
                        Utils.TYPE_JSON,
                        json
                );

                try {
                    URL imageUrl = new URL(content);
                } catch (MalformedURLException e) {
                    retries += 1;
                    setStatus2(String.format(getString(R.string.pre_image_status4), retries));
                    logError("Ao contactar o servidor");
                    return doInBackground();
                }

                logAction("Imagem gerada");

                Intent intent = new Intent(FinalizarActivity.this, FinalizadoActivity.class);
                intent.putExtra("url", content);
                startActivity(intent);
                finish();


                return null;
            }
        };

        task.execute();


    }

}

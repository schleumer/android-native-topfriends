package li.ues.topfriends.system;

import java.io.Serializable;

/**
 * Created by schleumer on 12/01/16.
 */
public class User implements Serializable {
    private String Id = null;
    private String FbId = null;
    private String ImageUrl = null;
    private String Name = null;
    private String ShortName = null;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFbId() {
        return FbId;
    }

    public void setFbId(String fbId) {
        FbId = fbId;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }
}

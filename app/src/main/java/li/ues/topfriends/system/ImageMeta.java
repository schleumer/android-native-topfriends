package li.ues.topfriends.system;

import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by schleumer on 12/01/16.
 */
public class ImageMeta implements Serializable {

    public ImageMeta(List<Thread> threads) {
        Collections.sort(threads, new Ordering<Thread>() {
            @Override
            public int compare(Thread left, Thread right) {
                return Longs.compare(left.getMessageCount(), right.getMessageCount());
            }
        }.reverse());

        this.Threads = new ArrayList<>();
        this.Threads.addAll(threads);

        this.RemovedThreads = new ArrayList<>();

    }

    private List<Thread> Threads = new ArrayList<Thread>();
    private List<Thread> RemovedThreads = new ArrayList<Thread>();

    public List<Thread> getThreads() {
        return Threads;
    }

    public void setThreads(List<Thread> threads) {
        Threads = threads;
    }

    public List<Thread> getRemovedThreads() {
        return RemovedThreads;
    }

    public void setRemovedThreads(List<Thread> removedThreads) {
        RemovedThreads = removedThreads;
    }
}

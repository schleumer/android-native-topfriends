package li.ues.topfriends.system;


import java.io.Serializable;
import java.util.List;

/**
 * Created by schleumer on 12/01/16.
 */
public class Thread implements Serializable {
    private String ThreadId = null;
    private String ThreadFbId = null;
    private String OtherUserId = null;
    private List<User> Participants = null;
    private Long MessageCount = null;
    private User OtherUser = null;

    public String getThreadId() {
        return ThreadId;
    }

    public void setThreadId(String threadId) {
        ThreadId = threadId;
    }

    public String getThreadFbId() {
        return ThreadFbId;
    }

    public void setThreadFbId(String threadFbId) {
        ThreadFbId = threadFbId;
    }

    public String getOtherUserId() {
        return OtherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        OtherUserId = otherUserId;
    }

    public List<User> getParticipants() {
        return Participants;
    }

    public void setParticipants(List<User> participants) {
        Participants = participants;
    }

    public Long getMessageCount() {
        return MessageCount;
    }

    public void setMessageCount(Long messageCount) {
        MessageCount = messageCount;
    }

    public User getOtherUser() {
        return OtherUser;
    }

    public void setOtherUser(User otherUser) {
        OtherUser = otherUser;
    }
}

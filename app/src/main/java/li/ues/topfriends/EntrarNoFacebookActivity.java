package li.ues.topfriends;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import li.ues.topfriends.utils.UserData;

/* An instance of this class will be registered as a JavaScript interface */
interface MyJavaScriptInterface
{
    @JavascriptInterface
    @SuppressWarnings("unused")
    void processHTML(String html);
}

class AsyncCheckHtml extends AsyncTask<String, Integer, UserData> {
    @Override
    protected UserData doInBackground(String... params) {
        return Utils.isUserOnHtml(params[0]);
    }

}

public class EntrarNoFacebookActivity extends GenericActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrar_no_facebook);

        ImageView logo = (ImageView) findViewById(R.id.logo);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.spin);

        logo.setAnimation(animation);

        //CookieSyncManager.createInstance(this);
        //CookieManager.getInstance().removeAllCookie();

        final WebView webview = (WebView)findViewById(R.id.entrarNoFacebookWebView);
        final TextView status = (TextView)findViewById(R.id.entrarNoFacebookTextViewBottom);
        final RelativeLayout loading = (RelativeLayout)findViewById(R.id.entrar_loading);

        final EntrarNoFacebookActivity self = this;

        webview.addJavascriptInterface(new MyJavaScriptInterface() {
            @Override
            @JavascriptInterface
            public void processHTML(String html) {
                AsyncCheckHtml task = new AsyncCheckHtml() {
                    @Override
                    protected void onPreExecute() {
                        self.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    status.setText(getString(R.string.entrar_pagina_carregada_procurando));
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                }
                            }
                        });
                        super.onPreExecute();
                    }

                    @Override
                    protected void onPostExecute(UserData data) {
                        if(data.isOk()) {
                            logAction("Usuario logado");

                            Intent userData = new Intent();
                            userData.putExtra("id", data.getId());
                            userData.putExtra("fbDtsg", data.getFbDtsg());
                            setResult(0xB16B00B5, userData);

                            finish();
                        } else {
                            self.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loading.setVisibility(View.GONE);
                                    try {
                                        status.setText(getString(R.string.entrar_pagina_carregada_aguardando));
                                    } catch (Exception ex) {
                                        System.out.println(ex.getMessage());
                                    }
                                }
                            });
                        }
                    }
                };

                task.execute(html);
            }
        }, "HTMLOUT");

        WebSettings ws = webview.getSettings();
        ws.setUserAgentString(Utils.UserAgent);
        ws.setJavaScriptEnabled(true);

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loading.setVisibility(View.VISIBLE);

                super.onPageStarted(view, url, favicon);
                status.setText(getString(R.string.entrar_pagina_carregando));
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                /*Bitmap bitmap = Bitmap.createBitmap(webview.getWidth(), webview.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                webview.draw(canvas);*/

                super.onPageFinished(view, url);
                if (android.os.Build.VERSION.SDK_INT < 21) {
                    CookieSyncManager.getInstance().sync();
                }
                status.setText(getString(R.string.entrar_pagina_carregada_aguardando));
                webview.loadUrl("javascript:HTMLOUT.processHTML(document.documentElement.outerHTML);");
            }
        });
        webview.loadUrl("https://m.facebook.com/");

    }

}

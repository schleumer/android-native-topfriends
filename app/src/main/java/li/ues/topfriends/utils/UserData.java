package li.ues.topfriends.utils;

import android.content.Intent;

public class UserData {
    public String Id;
    public String FbDtsg;
    public String Name;
    public String Alias;
    public String ProfilePicture;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFbDtsg() {
        return FbDtsg;
    }

    public void setFbDtsg(String fbDtsg) {
        this.FbDtsg = fbDtsg;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public UserData(Intent data) {
        this.Id = data.getStringExtra("id");
        if(this.Id == null) {
            this.Id = "";
        }

        this.FbDtsg = data.getStringExtra("fbDtsg");
        if(this.FbDtsg == null) {
            this.FbDtsg = "";
        }
    }

    public UserData(String id, String fbDtsg) {
        this.Id = id;
        this.FbDtsg = fbDtsg;
    }

    public Boolean isOk() {
        return Id.length() > 2 && FbDtsg.length() > 0;
    }

    public Boolean exists() {
        return Name != null && Name.length() > 0;
    }
}

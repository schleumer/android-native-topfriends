package li.ues.topfriends.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public static InputStream openUrlInputStream(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        if(urlConnection instanceof HttpURLConnection) {
            HttpURLConnection httpUrlConnection = (HttpURLConnection)urlConnection;
            httpUrlConnection.setInstanceFollowRedirects(false);
            int httpResponseCode = httpUrlConnection.getResponseCode();

            boolean redirected = ((httpResponseCode >= 300) && (httpResponseCode <=399));
            if(redirected) {
                String redirectedURLString = urlConnection.getHeaderField("Location");
                URL redirectedUrl;

                if(redirectedURLString.startsWith("http")) {
                    redirectedUrl = new URL(redirectedURLString);
                } else {
                    redirectedUrl = new URL(url, redirectedURLString);
                }

                return openUrlInputStream(redirectedUrl);
            }
        }
        return urlConnection.getInputStream();
    }

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = openUrlInputStream(new java.net.URL(urldisplay));
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}
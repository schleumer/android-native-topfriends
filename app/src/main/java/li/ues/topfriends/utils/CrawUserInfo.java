package li.ues.topfriends.utils;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import li.ues.topfriends.Utils;

public class CrawUserInfo extends AsyncTask<UserData, Integer, UserData> {

    @Override
    protected UserData doInBackground(UserData... params) {
        UserData data = params[0];
        String text = Utils.getUrl("https://m.facebook.com/chat/user_info/?ids[0]=" + data.Id);
        // Aw yeah
        if(text.contains(data.Id)) {
            try {
                text = text.replace("for (;;);", "").replace("for(;;);", "");

                JSONObject json = new JSONObject(text);
                JSONObject user = json
                        .getJSONObject("payload")
                        .getJSONObject("payload")
                        .getJSONObject("profiles")
                        .getJSONObject(data.Id);

                String userName = user.getString("name");
                String userAlias = user.getString("vanity");
                String userImage = "https://graph.facebook.com/" + data.Id + "/picture?width=256";

                if(userName != null && userAlias != null) {
                    data.setName(userName);
                    data.setProfilePicture(userImage);
                    data.setAlias(userAlias);
                }
                return data;
            } catch (JSONException e) {
                System.out.println(e.getMessage());
            }
        }
        return data;
    }
}

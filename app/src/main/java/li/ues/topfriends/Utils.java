package li.ues.topfriends;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import li.ues.topfriends.utils.UserData;
import li.ues.topfriends.utils.WebkitCookieManagerProxy;

public class Utils {
    public static String TYPE_JSON = "application/json";
    public static String TYPE_FORM = "application/x-www-form-urlencoded";
    public static String UserAgent = "Mozilla/5.0 (Linux; U; Android 4.4.4; en-in; XT1022 Build/KXC21.5-40) AppleWebKit/537.16 (KHTML, like Gecko) Version/4.0 Mobile Safari/537.16";
    public static String getUrl(String uri) {
        return getUrl(uri, 0);
    }

    public static void bootHttpClient() {
        WebkitCookieManagerProxy coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
        java.net.CookieHandler.setDefault(coreCookieManager);
    }


    /**
     * THIS IS SO WRONG
     * @param params
     * @return
     */
    private static String getQuery(HashMap<String, String> params)
    {
        try {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (Map.Entry<String, String> pair : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }

            return result.toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static String rawPostUrl(String uri, String type, String data) {
        bootHttpClient();
        URL url = null;

        try {
            url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("User-Agent", UserAgent);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", type);
            connection.setDoInput(true);
            connection.setConnectTimeout(10000);
            connection.setDoOutput(true);

            OutputStream os = connection.getOutputStream();
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(os));
            pw.write(data);
            pw.close();

            connection.connect();

            InputStreamReader in = new InputStreamReader((InputStream) connection.getContent());
            BufferedReader buff = new BufferedReader(in);
            String line;
            StringBuilder text = new StringBuilder();

            while ((line = buff.readLine()) != null) {
                text.append(line);
            }

            return text.toString();
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static String getUrl(String uri, Integer redirections){
        bootHttpClient();
        URL url = null;

        try {
            url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("User-Agent", UserAgent);
            connection.setConnectTimeout(5000);
            connection.connect();
            Integer status = connection.getResponseCode();
            Boolean redirect = false;
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER)
                    redirect = true;
            }

            if (redirect) {
                String nextLocation = connection.getHeaderField("Location");
                if(nextLocation == null) {
                    throw new MalformedURLException();
                } else {
                    return getUrl(nextLocation, redirections + 1);
                }
            }

            InputStreamReader in = new InputStreamReader((InputStream) connection.getContent());
            BufferedReader buff = new BufferedReader(in);
            String line;
            StringBuilder text = new StringBuilder();

            while ((line = buff.readLine()) != null) {
                text.append(line);
            }

            return text.toString();
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static UserData isUserOnHtml(String html) {
        String userId = "";
        String fbDtsg = "";

        try {
            Pattern p = Pattern.compile("\"USER_ID\":\"(.*?)\"");
            Matcher m = p.matcher(html);
            if (m.find()) {
                userId = m.group(1);
            }

            Pattern p2 = Pattern.compile("\"token\":\"(.*?)\"");
            Matcher m2 = p2.matcher(html);
            if (m2.find()) {
                fbDtsg = m2.group(1);
            }

            return new UserData(userId, fbDtsg);
        } catch (Exception ex) {
            System.out.println(ex);
            return new UserData("", "");
        }


    }

    public static String plural(Context ctx, Long count, int singular, int plural, int none) {
        if(count > 1) {
            return String.format(ctx.getString(plural), count);
        } else if (count == 1) {
            return String.format(ctx.getString(singular), count);
        } else {
            return String.format(ctx.getString(none), count);
        }
    }
}
